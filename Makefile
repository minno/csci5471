CCFLAGS= -arch=sm_20 -g 

all: fill_mem count_mem mem_graph find_ascii rsa

fill_mem: src/fill_mem.cu src/utils.h
	nvcc $< -o $@ $(CCFLAGS)

count_mem: src/count_mem.cu src/utils.h
	nvcc $< -o $@ $(CCFLAGS)

mem_graph: src/mem_graph.cu src/utils.h
	nvcc $< -o $@ $(CCFLAGS)

find_ascii: src/find_ascii.cu src/utils.h
	nvcc $< -o $@ $(CCFLAGS)

rsa: src/rsa.cu src/utils.h
	nvcc $< -o $@ $(CCFLAGS)

clean:
	rm fill_mem count_mem mem_graph find_ascii rsa
