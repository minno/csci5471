#include <cuda_runtime.h>

#include "utils.h"

#include <iostream>
#include <iomanip>
#include <stdint.h>

int main() {
	//auto start = std::chrono::high_resolution_clock::now();
	uint64_t prev_mem = 0;
	while (true) {
		uint64_t mem = get_free_mem();
		if (mem - prev_mem != 0) {
			//auto dt = std::chrono::high_resolution_clock::now() - start;
			std::cout << std::setw(20) << mem
			          << std::setw(20) << int64_t(mem - prev_mem) << '\n';
					  //<< std::setw(20) << dt.count() << '\n';
		}
		//std::this_thread::sleep_for(std::chrono::milliseconds(1));
		prev_mem = mem;
	}
}
