#include <cuda_runtime.h>

#include "utils.h"

#include <vector>
#include <algorithm>
#include <iterator>
#include <stdio.h>
#include <stdint.h>

using std::size_t;

#define NBLOCKS 256
#define THREADS_PER_BLOCK 256
#define NTHREADS (NBLOCKS * THREADS_PER_BLOCK)

__device__ bool isprint(uint8_t ch) {
	return ((ch >= 0x20) && (ch <= 0x7E)) || ch == '\n';
}

__global__ void find_ascii(uint8_t* mem, size_t size, 
                           uint8_t** chunks, size_t* sizes) {
	size_t thread_id = (blockDim.x * blockIdx.x + threadIdx.x);

	size_t lo = (size * thread_id) / NTHREADS;
	size_t hi = (size * (thread_id + 1)) / NTHREADS;

	bool in_chunk = false;
	size_t biggest_chunk_size = 0;
	size_t curr_chunk_size = 0;
	size_t chunk_loc = 0;
	size_t i;
	for (i = lo; i < hi && i < size; ++i) {
		if (in_chunk && isprint(mem[i])) {
			// Continue in chunk
			++curr_chunk_size;
		} else if (in_chunk && !isprint(mem[i])) {
			// End chunk
			if (curr_chunk_size > biggest_chunk_size) {
				biggest_chunk_size = curr_chunk_size;
				chunk_loc = i - curr_chunk_size;
			}
			in_chunk = false;
		} else if (!in_chunk && isprint(mem[i])) {
			// Begin chunk
			curr_chunk_size = 1;
			in_chunk = true;
		} else {
			// Continue out of chunk
		}
	}

	// Continue searching if we reached the end of this thread's chunk
	// and we're still in a chunk
	for (; i < size && in_chunk; ++i) {
		if (isprint(mem[i])) {
			++curr_chunk_size;
		} else {
			// End chunk
			if (curr_chunk_size > biggest_chunk_size) {
				biggest_chunk_size = curr_chunk_size;
				chunk_loc = i - curr_chunk_size;
			}
			in_chunk = false;
		}
	}
	
	chunks[thread_id] = &mem[chunk_loc];
	sizes[thread_id] = biggest_chunk_size;
}

void find_ascii() {
	// Set up output parameters
	size_t* d_sizes;
	CHECK(cudaMalloc(&d_sizes, sizeof(*d_sizes)*NTHREADS));
	CHECK(cudaMemset(d_sizes, 0, sizeof(*d_sizes)*NTHREADS));
	uint8_t** d_chunks;
	CHECK(cudaMalloc(&d_chunks, sizeof(*d_chunks)*NTHREADS));
	CHECK(cudaMemset(d_chunks, 0, sizeof(*d_chunks)*NTHREADS));

	// Get memory to read, uninitialized
	size_t free_mem = get_free_mem();
	size_t len = free_mem - (1 << 20);
	uint8_t* mem;
	CHECK(cudaMalloc(&mem, len));
	printf("%p\n", mem);

	find_ascii<<<NBLOCKS, THREADS_PER_BLOCK>>>(mem, len, d_chunks, 
	                                           d_sizes);
	CHECK(cudaGetLastError());

	std::vector<size_t> sizes(NTHREADS);
	CHECK(cudaMemcpy(sizes.data(), d_sizes, sizes.size()*sizeof(*d_sizes),
	                 cudaMemcpyDeviceToHost));
	size_t max_pos = std::distance(sizes.begin(),
	                               std::max_element(sizes.begin(),
	                                                sizes.end()));
	std::string ascii_chunk(sizes[max_pos], '\0');
	uint8_t* chunk_ptr;
	CHECK(cudaMemcpy(&chunk_ptr, &d_chunks[max_pos], sizeof(chunk_ptr),
	                 cudaMemcpyDeviceToHost));
	CHECK(cudaMemcpy(&ascii_chunk[0], chunk_ptr,
	                 sizes[max_pos], cudaMemcpyDeviceToHost));
	
	std::cout << "Thread: " << max_pos << std::endl;
	std::cout << "Size: " << sizes[max_pos] << std::endl;
	std::cout << "Data:\n" << ascii_chunk << std::endl;

	std::sort(sizes.begin(), sizes.end());
	std::reverse(sizes.begin(), sizes.end());

	std::cout << "Biggest block sizes: \n";
	for (int i = 0; i < 10; ++i) {
		std::cout << sizes[i] << ' ';
	}
	std::cout << '\n';

	CHECK(cudaFree(d_sizes));
	CHECK(cudaFree(d_chunks));
	CHECK(cudaFree(mem));
}

int main() {
	find_ascii();
}
