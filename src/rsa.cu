#include <cuda_runtime.h>
#include <stdio.h>
#include <stdint.h>
#include <cstring>
#include <vector>
#include "utils.h"


#define THREADS_PER_BLOCK 256
#define NBLOCKS 1024
#define MEM_PER_THREAD (16384/4)

#define NSIZE 1000000

/*Here, base is an element of the message, exponent = e, and den = n
 */
__device__ uint64_t mod(uint64_t base, uint64_t exponent, uint64_t den){
	uint64_t a=(base%den)*(base%den);
	uint64_t ret = 1;
	float size=(float)exponent/2;
	if(exponent==0){
		return base%den;
	}
	else{
		while(1){
			if(size>0.5){
				ret=(ret*a)%den;
				size=size-1.0;
			}
			else if(size==0.5){
				ret=(ret*(base%den))%den;
				break;
			}
			else{
				break;
			}
		}
		return ret;
	}
}

__global__ void rsa(uint64_t * num, uint64_t *key, uint64_t *den, uint64_t *result){
	uint64_t i = (blockDim.x * blockIdx.x + threadIdx.x);
	uint64_t temp;
	if(i<NSIZE){
		temp=mod(num[i], *key, *den);
		atomicExch((unsigned long long int *)&result[i], (unsigned long long)temp);
	}
}


/*It looks like this is hardcoded for nsize = 3...
 */
void rsa(uint64_t key, uint64_t den, uint64_t* num, uint64_t* res){
	int devcount;
	cudaGetDeviceCount(&devcount);
	printf("%d CUDA devices found\n", devcount);
	if(devcount>0){
		cudaSetDevice(1);
		uint64_t *dev_num, *dev_key, *dev_den;
		uint64_t *dev_res;
		CHECK(cudaMalloc((void**)&dev_num, NSIZE*sizeof(uint64_t)));
		CHECK(cudaMalloc((void**)&dev_key, sizeof(uint64_t)));
		CHECK(cudaMalloc((void**)&dev_den, sizeof(uint64_t)));
		CHECK(cudaMalloc((void**)&dev_res, NSIZE*sizeof(uint64_t)));
		CHECK(cudaMemcpy(dev_num, num, NSIZE*sizeof(uint64_t), cudaMemcpyHostToDevice));
		CHECK(cudaMemcpy(dev_key, &key, sizeof(uint64_t), cudaMemcpyHostToDevice));
		CHECK(cudaMemcpy(dev_den, &den, sizeof(uint64_t), cudaMemcpyHostToDevice));
		CHECK(cudaMemcpy(dev_res, res, NSIZE*sizeof(uint64_t), cudaMemcpyHostToDevice));
		rsa<<<NBLOCKS, THREADS_PER_BLOCK>>>(dev_num, dev_key, dev_den, dev_res);
		CHECK(cudaMemcpy(res, dev_res, NSIZE*sizeof(uint64_t), cudaMemcpyDeviceToHost));
		cudaFree(dev_num);
		cudaFree(dev_key);
		cudaFree(dev_den);
		cudaFree(dev_res);
		
	}
}

main(){
	//uint64_t num[NSIZE];
	//uint64_t res[NSIZE];
	//uint64_t res2[NSIZE];
	std::vector<uint64_t> num(NSIZE);
	std::vector<uint64_t> res(NSIZE);
	std::vector<uint64_t> res2(NSIZE);
	char string[8] = {'D', 'E', 'A', 'D', 'B', 'E', 'E', 'F'};
	for(uint64_t i = 0; i < NSIZE; i++){
		memcpy(&num[i], string, sizeof(uint64_t));
		res[i] = 1;
		res2[i] = 1;
	}
	printf("%llu \n", num[0]);
	uint64_t n = 131*137;
	uint64_t e  = 7;
	uint64_t d = 10103;
	rsa(e, n, num.data(), res.data());
	printf("Encryption: ");
	printf("%llu ", res[1]);
//	rsa(d, n, res.data(), res2.data());
	printf("Decryption: ");
	printf("%llu ", res2[1]);
	printf("\n");
}
