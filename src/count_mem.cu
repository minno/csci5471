#include <cuda_runtime.h>
#include <stdint.h>
#include "utils.h"

using std::size_t;

#define THREADS_PER_BLOCK 256

__global__ void count_mem(uint32_t* mem, size_t size, uint64_t* count_out) {
	size_t thread_id = (blockDim.x * blockIdx.x + threadIdx.x);
	size_t low = (thread_id * size) / THREADS_PER_BLOCK;
	size_t high = ((thread_id+1) * size) / THREADS_PER_BLOCK;
	uint64_t count = 0;
	for (size_t i = low; i < high && i < size; ++i) {
		if (mem[i] == 0xDEADBEEFU) {
			++count;
		}
	}
	atomicAdd((unsigned long long int*)count_out, 
	          (unsigned long long int)count);
}

void count_mem() {
	uint64_t count = 0;
	uint64_t* d_count;
	CHECK(cudaMalloc(&d_count, sizeof(*d_count)));
	CHECK(cudaMemcpy(d_count, &count, sizeof(count), 
	                 cudaMemcpyHostToDevice));

	size_t free_mem = get_free_mem();
	uint32_t* mem;
	size_t len = free_mem / sizeof(*mem) - (1 << 18);
	CHECK(cudaMalloc(&mem, len*sizeof(*mem)));
	std::cout << "Checking " << (len*sizeof(*mem)) << " bytes...";
	count_mem<<<1024, THREADS_PER_BLOCK>>>(mem, len, d_count);
	CHECK(cudaGetLastError());
	std::cout << "done\n";

	CHECK(cudaFree(mem));
	CHECK(cudaMemcpy(&count, d_count, sizeof(count), 
	                 cudaMemcpyDeviceToHost));
	CHECK(cudaFree(d_count));

	std::cout << "Found " << (count*sizeof(*mem)) << " bytes filled.\n";
	std::cout << "Ratio: " << (double(count)/len) << "\n";
}

int main() {
	count_mem();
}
