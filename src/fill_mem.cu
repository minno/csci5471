#include <iostream>
#include <stdint.h>

#include <cuda_runtime.h>

#include "utils.h"

using std::size_t;

#define THREADS_PER_BLOCK 256
#define NBLOCKS 1024
#define MEM_PER_THREAD (16384/4)

__global__ void fill_memory(uint32_t* mem, size_t size) {
	size_t thread_id = (blockDim.x * blockIdx.x + threadIdx.x);
	for (size_t i = thread_id*MEM_PER_THREAD; 
	     i < (thread_id+1)*MEM_PER_THREAD && i < size; ++i) {
		mem[i] = 0xDEADBEEFU;
	}
}

void fill_memory() {
	// Fills all memory with repeated '0xdeadbeef'
	size_t free_mem = get_free_mem();
	uint32_t* mem;
	size_t len = free_mem / sizeof(*mem) - (1 << 18);
	CHECK(cudaMalloc(&mem, len*sizeof(*mem)));

	std::cout << "Filling " << (len*sizeof(*mem)) << " bytes...";
	fill_memory<<<NBLOCKS, THREADS_PER_BLOCK>>>(mem, len);
	CHECK(cudaGetLastError());
	CHECK(cudaFree(mem));
	std::cout << "done\n";
}

int main() {
	fill_memory();
}
