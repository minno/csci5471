#include <cstddef>
#include <iostream>
#include <cuda_runtime.h>

#define CHECK(code) do { \
	cudaError_t err = code; \
    if (err != cudaSuccess) { \
		std::cerr << "Error in "; \
		std::cerr << __FILE__ << ":" << __LINE__ << '\n'; \
		std::cerr << "Code " << err; \
		std::cerr << ":" << cudaGetErrorName(err) << '\n'; \
		std::cerr << '\t' << cudaGetErrorString(err) << '\n'; \
		exit(1); \
	} \
} while (false)

inline std::size_t get_free_mem() {
	std::size_t free_mem;
	CHECK(cudaMemGetInfo(&free_mem, NULL));
	return free_mem;
}
